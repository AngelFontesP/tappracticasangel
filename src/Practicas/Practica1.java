package Practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Practica1 extends JFrame implements ActionListener{
    JTextField cajaTexto;
    Practica1(){
        this.setTitle("Practica 1");
        this.setSize(300, 200);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        this.setLayout(new FlowLayout());
        
        JButton boton1= new JButton();
        boton1.setText("¡Saludar!");
        
        JLabel etiqueta1 = new JLabel();
        etiqueta1.setText("Escribe un nombre");
        
        cajaTexto = new JTextField(20);
        
        
        boton1.addActionListener(this);
        
        this.add(etiqueta1);
        this.add(cajaTexto);
        this.add(boton1);
        
        
    }
    
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica1().setVisible(true);
            }
        });
    
}

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this,"Hola "+ cajaTexto.getText()); //To change body of generated methods, choose Tools | Templates.
    }
    
}

